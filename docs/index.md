# Assembly Instructions
The OpenFlexure Microscope is a 3D printable microscope, with a very precise mechanical translation stage. The microscope is highly customisable. Find about more about the microscope on the [OpenFlexure website](https://openflexure.org/projects/microscope).

These instructions will take you through how to assemble various configurations. They also describe how to make a [Sangaboard Compatible motor controller](workaround_motor_electronics/workaround_motor_electronics.md) if you can't get hold of an official one.

There are interactive 3D views of the [RMS](interactive_3d_view_rms.md) and [low cost](interactive_3d_view_low_cost.md) versions of the microscope.

## Microscope Configurations:

#### [High-resolution motorised microscope](high_res_microscope.md)
[![](images/MicroscopeBlenderTrio.png)](high_res_microscope.md)  
This configuration of the microscope uses a traditional microscope objective for highest image quality.

#### [Motorised microscope with low-cost optics](low_cost_microscope.md)
This configuration of the microscope uses the original lens from a Raspberry Pi camera module.

#### [Upright microscope](upright-microscope.md)

This configuration of the microscope has the objective above the sample rather than below. It is newer and less well tested than other versions of the microscope. If you have problems building it, please let us know on [GitLab](https://gitlab.com/openflexure/openflexure-microscope/-/issues) or on [our forum](https://openflexure.discourse.group/).

## Customising your microscope

The OpenFlexure Microscope is designed to be customisable. There are a number of customisation options already available as discussed on our [customisations and alternatives page](customisation.md).

If you want to get more hands on with customisation all source files are available. You can either [download a zip of the source files for this release](source.zip) or visit our [GitLab page](https://gitlab.com/openflexure/openflexure-microscope/) to see our ongoing development.
