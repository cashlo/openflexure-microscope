# Interactive 3D view of the OpenFlexure Microscope

Below is an interactive 3D view of the assembled microscope. This page shows RMS optics.

![A render of the completed microscope](gltf/complete_microscope_rms.glb)
