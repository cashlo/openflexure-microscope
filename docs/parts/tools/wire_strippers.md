# Wire strippers

When making up custom cable assemblies it is necessary to strip the insulation from the wires used.  There are many designs of wire strippers available: any standard tool that is capable of stripping one wire at a time should be fine for this purpose.