# 2.5 mm Ball-end Allen Key

It is important that the Allen key has a ball end:
![](../../diagrams/AllenKeyBall.png)