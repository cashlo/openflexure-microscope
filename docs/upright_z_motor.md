## Attach the z motor {pagestep}

>i There is no motor attached to the z-actuator on the main body 

* Attach a motor to the z-actuator of the separate z-actuator in the same way as they x and y actuators
* Feed the motor cable through the rectangular slot to the left of the z actuator on the main body
* Attach a cable tidy cap to the motor lugs of the z-actuator of the main body
