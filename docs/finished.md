# Assembly complete

Congratulations, you have now assembled your OpenFlexure Microscope.

## The completed microscope {pagestep}

![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}0.png)
![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}1.png)
![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}2.png)
![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}3.png)

Your completed microscope should now look like these pictures.  
There is also an [interactive 3D view](interactive_3d_view_{{var_optics, default:rms}}.md) of the finished microscope.

## Software set-up {pagestep}

Setting up the software is not yet included in these instructions.  It is detailed on the website, in the ["Install" page].  Using the software is described in the ["control" page].

["Install" page]: https://openflexure.org/projects/microscope/install
["control" page]: https://openflexure.org/projects/microscope/control