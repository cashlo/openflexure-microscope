# Interactive 3D view of the OpenFlexure Microscope

Below is an interactive 3D view of the assembled microscope. This page shows low cost optics using a Raspberry Pi camera module, and employing that camera module's lens as the microscope objective.

![A render of the completed microscope](gltf/complete_microscope_low_cost.glb)
